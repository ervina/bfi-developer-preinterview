package nasipadang;

import java.util.ArrayList;
import java.util.Collections;

//class Solution
public class Solution {
  public int solution(Integer[] a) {
    //loop and store negative values only
    ArrayList<Integer> negativeNumbers = new ArrayList<Integer>();
    for(int i=0; i<a.length; i++) {
    	if(a[i]<0) {
    		negativeNumbers.add(a[i]);
    	}
    }
    
    //sort desc
    Collections.sort(negativeNumbers, Collections.reverseOrder());
    
    //check on each element, compare to naive ordered index. get first prder mismatch
    for(int i=0; i<negativeNumbers.size(); i++) {
    	if(negativeNumbers.get(i)<-i-1) {
    		return -i-1;
    	}
    }
    return -negativeNumbers.size()-1;
  }
}