package nasipadang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import info.debatty.java.stringsimilarity.JaroWinkler;

@WebServlet("/submit")
public class rendang extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException
	{
		String input = request.getParameter("input");
		System.out.println(input);
		
		String url = "https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json";
		URLConnection connection = new URL(url).openConnection();
		connection.connect();
		InputStream is = connection.getInputStream();
		JSONParser parser = new JSONParser();
		Object obj;
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		List<String> similarCities = new ArrayList<String>();
		String line, jsonText;
		StringBuilder sb = new StringBuilder();
		
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			
			while((line = in.readLine()) != null) {
				sb.append(line.trim());
			}
			in.close();
			
			jsonText = sb.toString();
			System.out.println(jsonText);
			
			obj = parser.parse(jsonText);
			jsonArray = (JSONArray) obj;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (int i=0; i<jsonArray.size(); i++) {
			jsonObject = (JSONObject) jsonArray.get(i);
			if (jsonObject.get("country").equals("ID")) {
				String city = jsonObject.get("name").toString();
				
				if (input.toLowerCase().contains(city.toLowerCase())) {
					similarCities.add(city);
					System.out.println(jsonObject.toJSONString());
				}
				else {
					JaroWinkler jw = new JaroWinkler();
					if(jw.similarity(city.toLowerCase(), input.toLowerCase()) > 0.775 
							&&  input.length() == city.length()) {
						similarCities.add(city);
						System.out.println(jsonObject.toJSONString());
					}
				}
			}
		}
		
		request.setAttribute("isSentFromServer", true);
		request.setAttribute("originCity", input);
		request.setAttribute("cities", similarCities);
		
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	}
	
}
