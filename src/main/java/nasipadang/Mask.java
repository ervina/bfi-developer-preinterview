package nasipadang;

import java.util.Arrays;

//class Mask
public class Mask {
  public String mask(String original) {
	String result;
	char[] word;
	int wlength = 0;
	String[] words = original.split(" ");
	StringBuilder sb = new StringBuilder();
	
	for(int i=0; i<words.length; i++) {
		word = words[i].toCharArray();
		wlength = words[i].length();
		
		if(wlength > 2) {
			Arrays.fill(word, 1, wlength-1, '*');
		}
		
		sb.append(word);
		if(i<words.length-1) {
			sb.append(" ");
		}
		
	}
	
	result = sb.toString();
	return result;
  }
}