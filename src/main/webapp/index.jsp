<%@ page import="nasipadang.rendang" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>

<%-- <% String value = request.getAttribute("citiesJSON").toString(); %>
 --%>
<form action="submit" method="post">
	<label>Input</label>
	<input type="text" name="input" placeholder="Insert City Name">
	
	<input type="submit" value="submit">
	
	</br>
	</br>
	<c:if test="${isSentFromServer}">
		City Name: ${originCity}
		</br></br>
		
		City Found: </br>
		<c:if test="${empty cities}">
			No city found.
		</c:if>
	</c:if>
	
	<c:forEach items="${cities}" var="city">
		${city}</br>
	</c:forEach>
</form>
